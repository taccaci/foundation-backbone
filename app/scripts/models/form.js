/*global define, Backbone*/
'use strict';
define(['app'], function(App){
  var FormModel = Backbone.Model.extend({

  });

  App.Models.FormModel = FormModel;
  return FormModel;
});

/*global define, require*/
'use strict';
require.config({
  paths: {
    'jquery': '../components/jquery/jquery',
    'underscore': '../components/underscore/underscore',
    'backbone': '../components/backbone/backbone',
    'layoutmanager': '../components/layoutmanager/backbone.layoutmanager',
    'bootstrap': '../components/bootstrap/docs/assets/js/bootstrap',
    'moment': '../components/moment/moment',
    'handlebars': '../components/handlebars/handlebars',
    'backbone-agave': '../components/backbone-agave/backbone-agave',
    'backbone-agave-apps': '../components/backbone-agave/backbone-agave-apps',
    'backbone-agave-jobs': '../components/backbone-agave/backbone-agave-jobs',
    'backbone-agave-io': '../components/backbone-agave/backbone-agave-io',
    'backbone-agave-postit': '../components/backbone-agave/backbone-agave-postit',
    'backbone-agave-profile': '../components/backbone-agave/backbone-agave-profile',
    'file-saver': '../components/file-saver/FileSaver'
  },
  shim: {
    handlebars: {
      exports: 'Handlebars'
    },
    underscore: {
      exports: '_'
    },
    backbone: {
      deps: ['jquery', 'underscore'],
      exports: 'Backbone'
    },
    layoutmanager: {
      deps: ['backbone'],
      exports: 'layoutmanager'
    },
    bootstrap: {
      deps: ['jquery'],
      exports: 'jquery'
    },
    'backbone-agave': {
      deps: ['backbone'],
      exports: 'Backbone.Agave'
    },
    'backbone-agave-io': {
      deps: ['backbone', 'backbone-agave'],
      exports: 'Backbone.Agave.IO'
    },
    'backbone-agave-jobs': {
      deps: ['backbone', 'backbone-agave', 'backbone-agave-io'],
      exports: 'Backbone.Agave.Jobs'
    },
    'backbone-agave-apps': {
      deps: ['backbone', 'backbone-agave', 'backbone-agave-io', 'backbone-agave-jobs'],
      exports: 'Backbone.Agave.Apps'
    },
    'backbone-agave-postit': {
      deps: ['backbone', 'backbone-agave'],
      exports: 'Backbone.Agave.PostIt'
    },
    'backbone-agave-profile': {
      deps: ['backbone', 'backbone-agave'],
      exports: 'Backbone.Agave.Profile'
    }
  }
});

define([
  'app',
  'jquery',
  'bootstrap',
  'moment',
  'handlebars',
  'file-saver',
  'backbone-agave',
  'backbone-agave-io',
  'backbone-agave-jobs',
  'backbone-agave-apps',
  'backbone-agave-postit',
  'backbone-agave-profile',
  'models/message',
  'models/form',
  'views/app-views',
  'views/form-views',
  'views/util-views',
  'views/agave-auth',
  'views/agave-io',
  'views/agave-apps',
  'views/agave-postit',
  'views/agave-profile',
  'routers/default'
], function(App) {
  App.start();
});
/*global define, Backbone*/
'use strict';
define(['app'], function(App) {
  var DefaultRouter = Backbone.Router.extend({
    routes: {
      '': 'index',
      'auth': 'authIndex',
      'auth/login': 'authLogin',
      'auth/new': 'authNew',
      'auth/check': 'authCheck',
      'auth/active': 'authListActive',
      'auth/logout': 'authLogout',
      'apps/public': 'appsPublicList',
      'apps/shared': 'appsSharedList',
      'apps/:id': 'appsView',
      'io': 'ioBrowser',
      'io/:owner': 'ioBrowser',
      'io/:owner/*path': 'ioBrowser',
      'postit': 'currentPostIts',
      'postit/create': 'postItForm',
      'profile': 'myProfile',
      'profile/search': 'searchProfile'
    },

    index: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AppViews.Home());
      App.Layouts.main.render();
    },

    authLogin: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveAuth.NewTokenForm({model: App.Agave.token()}));
      App.Layouts.main.render();
    },

    authNew: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveAuth.NewTokenForm({model: new Backbone.Agave.Auth.Token()}));
      App.Layouts.main.render();
    },

    authListActive: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveAuth.ActiveTokens({collection: new Backbone.Agave.Auth.ActiveTokens()}));
      App.Layouts.main.render();
    },

    authLogout: function() {
      App.Agave.destroyToken();
      window.localStorage.removeItem('Agave.Token');
      App.router.navigate('', {trigger:true});
    },

    appsPublicList: function() {
      App.Layouts.main.template = 'two-col';
      App.Layouts.main.setView('.columns-header', new App.Views.Util.Message({
        model: new App.Models.MessageModel({body:'Public Applications'}),
        tagName: 'h1'
      }));
      App.Layouts.main.setView('.sidebar', new App.Views.AgaveApps.AppList({collection: new Backbone.Agave.Apps.PublicApplications()}));
      App.Layouts.main.setView('.content', new App.Views.Util.Alert({model: new App.Models.MessageModel({
        header: 'Choose an application',
        body: 'Select an application to view it\'s details.'
      })}));
      App.Layouts.main.render();
    },

    appsSharedList: function() {
      App.Layouts.main.template = 'two-col';
      App.Layouts.main.setView('.columns-header', new App.Views.Util.Message({
        model: new App.Models.MessageModel({body:'My Shared Applications'}),
        tagName: 'h1'
      }));
      App.Layouts.main.setView('.sidebar', new App.Views.AgaveApps.AppList({collection: new Backbone.Agave.Apps.SharedApplications()}));
      App.Layouts.main.setView('.content', new App.Views.Util.Alert({model: new App.Models.MessageModel({
        header: 'Choose an application',
        body: 'Select an application to view it\'s details.'
      })}));
      App.Layouts.main.render();
    },

    appsView: function(id) {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveApps.AppView({model: new Backbone.Agave.Apps.Application({id:id})}));
      App.Layouts.main.render();
    },

    ioBrowser: function(owner, path) {
      App.Layouts.main.template = 'one-col';
      var fullPath = owner || App.Agave.token().get('username');
      if (path) {
        fullPath += '/' + path;
      }
      App.Layouts.main.setView('.content',new App.Views.AgaveIO.Browser({collection: new Backbone.Agave.IO.Listing([], {path: fullPath})}));
      App.Layouts.main.render();
    },

    currentPostIts: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgavePostIt.List());
      App.Layouts.main.render();
    },

    postItForm: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgavePostIt.CreateForm());
      App.Layouts.main.render();
    },

    myProfile: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveProfile.MyProfile());
      App.Layouts.main.render();
    },

    searchProfile: function() {
      App.Layouts.main.template = 'one-col';
      App.Layouts.main.setView('.content', new App.Views.AgaveProfile.Search({
        collection: new Backbone.Agave.Profile.Search()
      }));
      App.Layouts.main.render();
    }
  });

  App.Routers.DefaultRouter = DefaultRouter;
  return DefaultRouter;
});
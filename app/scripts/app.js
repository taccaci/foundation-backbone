/*global define, Backbone, $, _*/
'use strict';
define(['handlebars', 'backbone', 'layoutmanager'], function(Handlebars) {
  var App = {
    root: '/',
    init: function() {
      var JST = window.JST = window.JST || {};
      // Configure LayoutManager with Backbone Boilerplate defaults.
      Backbone.Layout.configure({
        // Allow LayoutManager to augment Backbone.View.prototype.
        manage: true,

        prefix: 'templates/',

        fetch: function(path) {
          // Concatenate the file extension.
          path = path + '.html';

          // If cached, use the compiled template.
          if (JST[path]) {
            return JST[path];
          }

          // Put fetch into `async-mode`.
          var done = this.async();

          // Seek out the template asynchronously.
          $.get(path, function(contents) {
            done(JST[path] = Handlebars.compile(contents));
          }, 'text');
        },
        render: function(tmpl, context) {
          return tmpl(context);
        }
      });

      // setup agave
      App.Agave = new Backbone.Agave({token: JSON.parse(window.localStorage.getItem('Agave.Token'))});
      var warn, error, watchToken = function() {
        clearTimeout(warn);
        clearTimeout(error);
        var token = App.Agave.token(), logoutWarningDialog;
        if (token.isValid()) {
          window.localStorage.setItem('Agave.Token', JSON.stringify(token.toJSON()));
          warn = setTimeout(function() {
            var logoutWarningDialog = new App.Views.Util.ModalView({
              model: new App.Models.MessageModel({header:'Your login session is about to expire!'})
            }),
            renewView = new App.Views.AgaveAuth.RenewTokenForm({
              model: App.Agave.token()
            });
            renewView.cleanup = function() {
              logoutWarningDialog.close();
              if (token.expiresIn() > 300000) {
                // it was renewed, rewatch token
                watchToken();
              }
            };
            logoutWarningDialog.setView('.child-view', renewView);
            logoutWarningDialog.$el.on('hidden', function() {
              logoutWarningDialog.remove();
              logoutWarningDialog = null;
            });
            logoutWarningDialog.render();
          }, Math.max(0, token.expiresIn() - 300000));

          error = setTimeout(function() {
            if (logoutWarningDialog) {
              logoutWarningDialog.close();
            }
            window.alert('Your Session has expired.  You have been logged out.');
            App.Agave.destroyToken();
            window.localStorage.removeItem('Agave.Token');
            App.router.navigate('', {'trigger':true});
          }, Math.max(0, token.expiresIn()));
        }
      };
      App.listenTo(App.Agave, 'Agave:tokenChanged', watchToken, this);
      App.listenTo(App.Agave, 'Agave:tokenDestroy', watchToken, this);
      watchToken();

      // initialize router, views, data and layouts
      App.Layouts.header = new App.Views.AppViews.HeaderLayout({
        el: '#header',
        views: {
          '': [
            new App.Views.AppViews.Nav(),
            new App.Views.AppViews.Header({model: App.Agave.token()})
          ]
        }
      });

      App.Layouts.main = new App.Views.AppViews.MainLayout({
        el: '#main'
      });

      App.Layouts.footer = new App.Views.AppViews.FooterLayout({
        el: '#footer',
        views: {
          '': new App.Views.AppViews.Footer()
        }
      });

      _.each(App.Layouts, function(layout) {
        layout.render();
      });

      App.router = new App.Routers.DefaultRouter();
      App.router.navigate('');
    },
    start: function() {
      App.init();
      Backbone.history.start();
      // Backbone.history.start({pushState: true, root: App.root});
      $(document).on('click', 'a[href]:not([data-bypass])', function(evt) {
        var href = { prop: $(this).prop('href'), attr: $(this).attr('href') };
        var root = location.protocol + '//' + location.host + App.root;
        if (href.prop.slice(0, root.length) === root) {
          evt.preventDefault();
          Backbone.history.navigate(href.attr, true);
        }
      });
    },
    Layouts: {},
    Views: {},
    Models: {},
    Collections: {},
    Routers: {}
  };
  return _.extend(App, {
    isLoggedIn: function() {
      return this.Agave.token().isValid();
    }
  }, Backbone.Events);
});

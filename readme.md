Foundation Backbone
===================

This is a reference implementation of the iPlant Foundation API using Backbone.js.

## Setup and installation

### Requirements

The following are not requirements to run the application, but are used in the build process
and for other tasks during development

- node.js + npm (v0.8.18+)
- yeoman
- grunt-cli (0.1.7+)
- bower (0.8.6+)
- ruby + rubygems (required for sass + compass)
- sass (3.2.7+)
- compass (0.12.2)

#### Node.js

Node and NPM are used for installing packages necessary for the rest of the build process.
Install node.js and npm via whatever install process is available.

http://nodejs.org/download/

#### Yeoman, Grunt, Bower

After you have installed node and npm, you can install yeoman, grunt, and bower.

    npm install -g yo grunt-cli bower

#### Sass + Compass

Make sure you have ruby and rubygems installed on your system.

    gem install sass compass


### Project setup

Check out the project and cd into the project.  Use npm and bower to install the rest of the project
dependencies.

    git clone git@bitbucket.org:taccaci/foundation-backbone.git
    cd foundation-backbone
    npm install
    bower install

#### Start the application in node development server

This will start a node server on localhost:9000 and automatically open the default browser to the application.  Using this for development

    grunt server

#### Building releases

There are two release modes available, production and human-friendly.  There are builds of the application
that can be dropped into a webserver, a content delivery network (CDN), or even served straight out of Dropbox
or Google drive!

You can build a production release (catted, minified, and uglified) with

    grunt

This will build the application in {{project_home}}/dist.


You can build a human-friendly release (expanded and readable) with

    grunt debug

This will build the application in {{project_home}}/debug.  Note, this build is **not production-friendly**.
Resources such as templates and module dependencies are fetched synchronously, so the application will occasionally